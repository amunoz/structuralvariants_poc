process EXOME_DEPTH {
  tag "exome depth"
  container 'quay.io/biocontainers/r-exomedepth:1.1.12--r36h6786f55_0'

  input:
    tuple path(input), path(bai) 
    path mapping
    tuple path(index_files), path(fai), path(reference_genome)
    path script

  output:
    path "batch_*.exomeDepth.csv", emit: output

  script:
  """
  Rscript $script $input $mapping $reference_genome
  """
}