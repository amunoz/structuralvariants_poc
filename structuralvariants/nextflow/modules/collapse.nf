process COLLAPSE {
  tag { "collapse "}
  container 'quay.io/biocontainers/bedtools:2.26.0gx--he513fc3_4'

  publishDir "${params.outputDir}", mode: 'copy'

  input:
    path input

  output:
    path "*.bed"

  script:
  template "collapse.sh"
}
