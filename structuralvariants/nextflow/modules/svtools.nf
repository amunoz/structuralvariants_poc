process SVTOOLS {
  tag { "svtools" }
  container 'quay.io/biocontainers/svtools:0.5.1--py_0'

  input:
    path input

  output:
    path "*.manta.raw.bed", emit: output

  script:
  """
  for i in $input
  do
    svtools vcftobedpe -i \$i -o \$(echo \${i%.*} | sed 's/vcf/bed/g')
  done
  """
}
