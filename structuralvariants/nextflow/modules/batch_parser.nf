process BATCH_PARSER {
  tag "batch parser"
  container 'r-base:latest'

  input:
    path input
    path samples
    path script

  output:
    path "bams_x_batch_*.csv", emit: output

  script:
  """
  Rscript $script $input $samples
  """
}