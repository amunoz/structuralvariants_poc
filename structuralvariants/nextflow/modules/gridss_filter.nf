process GRIDSS_FILTER {
  tag { "gridss filter" }

  input:
    path input
    path sample
    val max_len
    val min_len
    val min_q
    
  output:
    path "*"

  script:
  """
  for i in $input
  do
    sh $projectDir/templates/gridss_filter.sh \$i $sample $min_len $max_len $min_q
  done
  """
}
