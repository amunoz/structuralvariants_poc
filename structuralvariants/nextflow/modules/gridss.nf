process GRIDSS {
  tag { "gridss" }
  container 'quay.io/biocontainers/gridss:2.9.3--0'

  input:
    tuple path(bams), path(bais)
    path reference_genome
    path indexs
    path blacklist    

  output:
    path "*.gridss.raw.vcf.gz"

  script:
  def threadsArgument = params.threads_gridss ? "--threads $params.threads_gridss" : ""
  """
  for i in $bams
  do
    srr=\$(echo \$i | cut -f 1 -d '.')

    gridss --reference $reference_genome \\
           --output \$(echo \${i%%.*}).gridss.raw.vcf.gz \\
           --assembly "\$(echo \$srr).$params.assemblyFilename" \\
           $threadsArgument \\
           --jar "/usr/local/share/gridss-2.9.3-0/gridss.jar" \\
           --blacklist $blacklist \\
           \$i
  done
  """
}
