process STRUCTURAL_VARIANTS {
  tag { "structural variants" }
  container 'quay.io/biocontainers/bioconductor-structuralvariantannotation:1.6.0--r40_0'

  input:
    path input
    path script

  output:
    path "*.bed", emit: output

  script:
  """
  for i in $input
  do
    Rscript $script \$i 
  done
  """
}