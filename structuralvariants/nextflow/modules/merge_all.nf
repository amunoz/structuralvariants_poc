process MERGE_ALL {
  tag { "merge all" }
  container 'quay.io/biocontainers/bedtools:2.26.0gx--he513fc3_4'

  input:
    path input

  output:
    path "*.bed"

  script:
  template "merge_all.sh"
}