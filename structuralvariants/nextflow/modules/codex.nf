process CODEX {
  tag "codex"
  container 'migbro/codex2:3.8'

  input:
    tuple path(input), path(bai)
    path mapping
    path bed
    path script

  output:
    path "*.CODEX_Chrom.txt", emit: output

  script:
  """
  Rscript $script $input $mapping $bed $params.chromosome
  """ 
}