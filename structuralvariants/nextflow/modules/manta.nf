process MANTA {
  tag { "manta" }
  container 'quay.io/biocontainers/manta:1.6.0--py27_0'

  input:
    tuple path(bams), path(bais)
    path reference_genome
    path indexs
    path bed
    path bed_tbi
    val exome

  output:
    tuple path("*.manta.raw.vcf.gz"), emit: output

  script:
  def exomeArgument = exome ? "--exome" : ""
  """
  for i in $bams
  do
    srr=\$(echo \$i | cut -f 1 -d '.')
    configManta.py --bam \$i --referenceFasta $reference_genome --runDir generated\$(echo \$srr) --callRegions $bed $exomeArgument
    python generated\$(echo \$srr)/runWorkflow.py
    cp generated\$(echo \$srr)/results/variants/diploidSV.vcf.gz ./\$(echo \$srr).manta.raw.vcf.gz
  done
  """
}