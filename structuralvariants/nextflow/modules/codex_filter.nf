process CODEX_FILTER {
  tag { "codex filter" }

  input:
    path input
    path sample
    path script
    val max_len
    val min_len
    val min_lratio

  output:
    path "*.bed"

  script:
  """
  sh $projectDir/templates/codex_filter.sh $input $samples $min_len $max_len $min_lratio
  """
}