#!/usr/bin/env nextflow

nextflow.enable.dsl = 2

/*
 * Channels
*/
Channel
  .fromFilePairs( params.fastq, flat:true )
  .set { fastq }
Channel
  .fromPath( params.reference_fasta )
  .set{ reference_fasta }
Channel
  .fromPath( params.samples )
  .set{ samples }
Channel
  .fromPath( params.bed )
  .set{ bed }
Channel
  .fromPath( params.tbi )
  .set{ tbi }
Channel
  .fromPath( params.blacklist )
  .set{ blacklist }
Channel
  .fromPath( params.manta_filter_script )
  .set{ manta_filter_script }
Channel
  .fromPath( params.gridss_filter_script )
  .set{ gridss_filter_script }
Channel
  .fromPath( params.structural_variants_script )
  .set{ structural_variants_script }
Channel
  .fromPath( params.batch_parser_script )
  .set{ batch_parser_script }
Channel
  .fromPath( params.exomedepth_filter_script )
  .set{ exomedepth_filter_script }
Channel
  .fromPath( params.exomeDepth_script )
  .set{ exomeDepth_script }
Channel
  .fromPath( params.codex_script )
  .set{ codex_script }
Channel
  .fromPath( params.codex_filter_script )
  .set{ codex_filter_script }
Channel
  .fromPath( params.collapse_script )
  .set{ collapse_script }
Channel
  .fromPath( params.merge_all_script )
  .set{ merge_all_script }

/*
 * Define the default parameters
*/
params.threads_fastqc = 1
params.threads_fastp = 1
params.threads_bwa_mem = 1
params.threads_samtools = 1
params.threads_gridss = 1
params.cut_right = true
params.cut_right_window_size = 5
params.cut_right_mean_quality = 24
params.trim_tail1 = 1
params.length_required = 70
params.min_mapping_quality = 10
params.bits_set = 4
params.algoType = ''
params.assemblyFilename = "gridss.assembly.bam"
params.read_group = "@RG\\tID:SRR709972\\tSM:NA19206\\tPL:ILLUMINA\\tCN:CBRA\\tLB:Fragment"
params.manta_exome = true

/*
 * Include subworkflows
*/
include { SUB_BWA_INDEX } from './subworkflows/bwa_index'
include { SUB_TRIMMED_FASTQ } from './subworkflows/trimmed_fastq'
include { SUB_BWA_MEM } from './subworkflows/bwa_mem'
include { SUB_SAMTOOLS_VIEW_SAM2BAM } from './subworkflows/samtools_view_sam2bam'
include { SUB_SAMTOOLS_SORT } from './subworkflows/samtools_sort'
include { SAMTOOLS_MERGE } from './modules/samtools_merge'
include { SAMTOOLS_INDEX } from './modules/samtools_index'
include { SUB_PICARD_MARKDUPLICATES } from './subworkflows/picard_markduplicates'
include { SUB_BAM_FILTERING } from './subworkflows/bam_filtering'
include { SUB_CNV_MANTA } from './subworkflows/cnv_manta'
include { SUB_CNV_GRIDSS } from './subworkflows/cnv_gridss'
include { SUB_CNV_EXOMEDEPTH } from './subworkflows/cnv_exomedepth'
include { SUB_CNV_CODEX } from './subworkflows/cnv_codex'
include { SUB_FINAL_FILTERING } from './subworkflows/final_filtering'

workflow {
  if( params.generate_bwa_indexes ) {
    output_bwa_index = SUB_BWA_INDEX(reference_fasta)
    indexFiles = output_bwa_index.indexs.combine(output_bwa_index.fai).collect()
  }
  else {
    indexChr = Channel.fromPath(params.reference_fasta_indexs)
    indexFiles = indexChr.collect()
  }

  SUB_TRIMMED_FASTQ(fastq)

  SUB_BWA_MEM(
    SUB_TRIMMED_FASTQ.out.reads,
    indexFiles
  )

  SUB_SAMTOOLS_VIEW_SAM2BAM(
    SUB_BWA_MEM.out.paired,
    SUB_BWA_MEM.out.unpairedR1,
    SUB_BWA_MEM.out.unpairedR2
  )

  SUB_SAMTOOLS_SORT(
    SUB_SAMTOOLS_VIEW_SAM2BAM.out.paired,
    SUB_SAMTOOLS_VIEW_SAM2BAM.out.unpairedR1,
    SUB_SAMTOOLS_VIEW_SAM2BAM.out.unpairedR2
  )

  SAMTOOLS_MERGE(
    SUB_SAMTOOLS_SORT.out.paired,
    SUB_SAMTOOLS_SORT.out.unpairedR1,
    SUB_SAMTOOLS_SORT.out.unpairedR2
  )

  SAMTOOLS_INDEX(
    SAMTOOLS_MERGE.out.output
  )

  SUB_PICARD_MARKDUPLICATES(
    SUB_SAMTOOLS_INDEX.out.output
  )

  SUB_BAM_FILTERING(
    SUB_PICARD_MARKDUPLICATES.out.alignments
  )

  if( params.enable_manta )
    SUB_CNV_MANTA (
      SUB_BAM_FILTERING.out.collect().toSortedList(),
      samples,
      reference_fasta,
      indexFiles,
      bed,
      tbi,
      params.manta_exome,
      manta_filter_script,
      collapse_script,
      params.manta_max_len,
      params.manta_min_len,
      params.manta_min_q
    )

  if( params.enable_gridss )
    SUB_CNV_GRIDSS (
      SUB_BAM_FILTERING.out.collect().toSortedList(),
      samples,
      reference_fasta,
      indexFiles,
      blacklist,
      structural_variants_script,
      gridss_filter_script,
      collapse_script,
      params.gridss_max_len,
      params.gridss_min_len,
      params.gridss_min_q
    )

  if( params.enable_exomeDepth )
    SUB_CNV_EXOMEDEPTH (
        SUB_BAM_FILTERING.out.collect().toSortedList(),
        samples,
        indexFiles,
        batch_parser_script,
        exomeDepth_script,
        exomedepth_filter_script,
        collapse_script,
        params.exomedepth_min_len, 
        params.exomedepth_max_len,
        params.exomedepth_min_bf
    )

  if( params.enable_codex )
    SUB_CNV_CODEX (
        SUB_BAM_FILTERING.out.collect().toSortedList(),
        samples,
        bed,
        params.codex_max_len
        params.codex_min_len 
        params.codex_min_lratio
    )

  SUB_FINAL_FILTERING(final_filtering)
}