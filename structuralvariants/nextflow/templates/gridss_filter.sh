#!/bin/bash
INPUT_FILE=$1
SAMPLE_FILE=$2
MIN_LEN=$3
MAX_LEN=$4
MIN_Q=$5
OUTPUT_NAME=$(echo "$INPUT_FILE" | sed "s/raw/filtered/")
OUTPUT_FILE=$(echo "$OUTPUT_NAME" | sed "s/.*\///")

# mapping case_id with sample_id
SAMPLE_ID=$(echo "$OUTPUT_FILE" | cut -d '.' -f 1)
CASE_ID=$(awk -v search="$SAMPLE_ID" '$0 ~ search{print $1}' "$SAMPLE_FILE")

tail -n +2 $INPUT_FILE | awk -v maxLen=$MAX_LEN -v minLen=$MIN_LEN -v minQ=$MIN_Q '{ \
chr=$1; \
start=$2; \
end=$6; \
sample="'$CASE_ID'"; \
q=$11; \
len=(end-start)/1000; \
type=$7; \
tool="gridss"; \
if(len>minLen && \
    len<maxLen && \
    q>=minQ && \
    (type=="DEL" || type=="DUP")){
      print chr"\t"start"\t"end"\t"sample"\t"type"\t"q"\t"tool}}' > ${OUTPUT_FILE}
