#!/bin/bash
INPUT_FILE=$input
OUTPUT_NAME=\$(echo "\$INPUT_FILE" | sed 's/bed/sorted.merged.bed/g')
OUTPUT_FILE=\$(echo "\$OUTPUT_NAME" | sed "s/.*\\///")

for i in \$(cat \$INPUT_FILE | sed -e "s/[[:space:]]\\+/\\t/g" | cut -f4 | sort -u)
do
  for j in "DEL" "DUP"
  do
    grep \$i \$INPUT_FILE | grep \$j | sed -e "s/[[:space:]]\\+/\\t/g" | bedtools sort | bedtools merge -c 6,7 -o max,distinct | awk -v sample=\$i -v type=\$j '{ \\
      print \$1,\$2,\$3,sample,type,\$4,\$5}' OFS="\\t"
  done >> \${OUTPUT_FILE}
done