/*
 * Includes for bwa_index
 */
include {
  BWA_INDEX
} from "../modules/bwa_index"
include {
  SAMTOOLS_FAIDX
} from "../modules/samtools_faidx"
include {
  GUNZIP
} from "../modules/gunzip"

/*
* Subworkflow bwa_index
*/
workflow SUB_BWA_INDEX {
  take:
    reference_fasta

  main:

    GUNZIP(reference_fasta)
    uncompressed = GUNZIP.out

    BWA_INDEX(uncompressed)
    SAMTOOLS_FAIDX(BWA_INDEX.out.reference_fasta)

  emit:
    fai = SAMTOOLS_FAIDX.out
    indexs = BWA_INDEX.out.indexs
}