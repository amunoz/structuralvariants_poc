/*
 * Include for cnv_exomedepth
*/
include {
    BATCH_PARSER
}from "../modules/batch_parser"
include {
    EXOME_DEPTH
}from "../modules/exome_depth"
include {
    EXOMEDEPTH_FILTER
}from "../modules/exomedepth_filter"
include {
    BEDOPS_UNION as EXOMEDEPTH_UNION
} from "../modules/bedops_union"
include {
    COLLAPSE as EXOMEDEPTH_MERGE
} from "../modules/collapse"

/*
* Subworkflow cnv_exomedepth
*/
workflow SUB_CNV_EXOMEDEPTH {
    take:
        bams
        samples
        reference_genome
        max_len
        min_len
        min_bf

    main:
        BATCH_PARSER( bams, samples, batch_parser_script )
        EXOME_DEPTH( bams, BATCH_PARSER.out, reference_genome, exomeDepth_script )
        EXOMEDEPTH_FILTER( exomedepth_filter_script, EXOME_DEPTH.out, samples, max_len, min_len, min_bf )
        EXOMEDEPTH_UNION( EXOMEDEPTH_FILTER.out )
        EXOMEDEPTH_MERGE( collapse_script, EXOMEDEPTH_UNION.out )
        
    emit:
        output = EXOMEDEPTH_MERGE.out
}