/*
 * Include for cnv_gridss
*/
include {
    GRIDSS
}from "../modules/gridss"
include {
    STRUCTURAL_VARIANTS
}from "../modules/structural_variants"
include {
    GRIDSS_FILTER
}from "../modules/gridss_filter"
include {
    BEDOPS_UNION as GRIDSS_UNION
} from "../modules/bedops_union"
include {
    COLLAPSE as GRIDSS_MERGE
} from "../modules/collapse"


/*
* Subworkflow cnv_gridss
*/
workflow SUB_CNV_GRIDSS {
    take:
        bams
        samples
        reference_genome
        indexs
        blacklist
        structural_variants_script
        gridss_filter_script
        collapse_script
        max_len
        min_len
        min_q

    main:
        GRIDSS(bams,
               reference_genome,
               indexs,
               blacklist)
        STRUCTURAL_VARIANTS( GRIDSS.out, structural_variants_script )
        GRIDSS_FILTER( STRUCTURAL_VARIANTS.out, samples, max_len, min_len, min_q )
        GRIDSS_UNION( GRIDSS_FILTER.out )
        GRIDSS_MERGE( GRIDSS_UNION.out )

    emit:
        output = GRIDSS_MERGE.out

}