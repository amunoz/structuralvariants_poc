/*
 * Include for cnv_manta
*/
include {
    MANTA
} from "../modules/manta"
include {
    SVTOOLS
} from "../modules/svtools"
include {
    MANTA_FILTER
} from "../modules/manta_filter"
include {
    BEDOPS_UNION as MANTA_UNION
} from "../modules/bedops_union"
include {
    COLLAPSE as MANTA_MERGE
} from "../modules/collapse"

/*
* Subworkflow cnv_manta
*/
workflow SUB_CNV_MANTA {
    take:
       bams
       bais
       samples
       reference_genome
       indexs
       bed
       bed_tbi
       exome
       manta_filter_script
       collapse_script
       max_len
       min_len
       min_q


    main:
        MANTA(bams, bais, reference_genome, indexs, bed, bed_tbi, exome)
        SVTOOLS(MANTA.out)
        MANTA_FILTER(manta_filter_script, SVTOOLS.out, samples, max_len, min_len, min_q)
        MANTA_UNION(MANTA_FILTER.out)
        MANTA_MERGE(collapse_script, MANTA_UNION.out)

    emit:
      output = MANTA_MERGE.out
}
