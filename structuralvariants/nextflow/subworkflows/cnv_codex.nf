/*
 * Include for cnv_codex
*/
include {
    CODEX
}from "../modules/codex"
include {
    CODEX_FILTER
}from "../modules/codex_filter"
include {
    BEDOPS_UNION as CODEX_UNION
} from "../modules/bedops_union"
include {
    COLLAPSE as CODEX_MERGE
} from "../modules/collapse"

/*
* Subworkflow cnv_codex
*/
workflow SUB_CNV_CODEX {
    take:
        bams
        samples
        bed
        max_len
        min_len 
        min_lratio
    main:
        BATCH_PARSER( bams, samples, batch_parser_script )
        CODEX( bams, BATCH_PARSER.out, bed, codex_script)
        CODEX_FILTER( CODEX.out, samples, codex_filter_script, max_len, min_len, min_lratio )
        CODEX_UNION( CODEX_FILTER.out )
        CODEX_MERGE( collapse_script, CODEX_UNION.out )

    emit:
        output = CODEX_MERGE.out
}